data_name='LasercutMS';
sensor_spacing=100;

data=eval(data_name);

channels=data.AEdata(:,2);
times=data.AEdata(:,1);

categories={"Grips1";"Grips2";"FinalFracture";"PrelimFracture";"FinalFractureGrips"};

% If no pair, assign to Grip category
% If a pair during test, assign to PrelimFracture
% If a pair during failure, assign to FinalFracture
% If no pair during failure, assign to FinalFractureGrips

% Identify location of PrelimFracture and FinalFracture

j=1;

for i=1:length(times)
    if channels(i)==1 && channels(i+1)==1
        assignments(i)=categories(1,:);
    elseif channels(i)==2 && channels(i+1)==2
        assignments(i)=categories(2,:);
    elseif channels(i)==1 && channels(i+1)==2
        assignments(i)=categories(4,:);
        fractures(j)=i;
        j=j+1;
    elseif channels(i)==2 && channels(i+1)==1
        assignments(i)=categories(4,:);
        fractures(j)=i;
        j=j+1;
    end
end

